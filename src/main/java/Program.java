package main.java;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.*;

public class Program {
    public static void main(String[] args) {
        GetExample();
        PostExample();
    }

    public static void GetExample(){
        try {
            URL url = new URL("https://reqres.in/api/users/2");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            if(con.getResponseCode() == HttpURLConnection.HTTP_OK){

                InputStreamReader isr = new InputStreamReader(con.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String inputLine;
                StringBuffer content = new StringBuffer();

                while ((inputLine = br.readLine()) != null) {
                    content.append(inputLine);
                }
                br.close();

                // print all content as plain text
                System.out.println(" --- String format --- ");
                System.out.println(content);
                System.out.println(" --------------------- \n");

                // print content formatted as JSON
                System.out.println(" ---  JSON format --- ");
                JSONObject json = new JSONObject(content.toString());
                System.out.println(json.toString(4));
                System.out.println(" --------------------- \n");

                // print data portion of JSON
                System.out.println(" ---   Data Only   --- ");
                System.out.println(json.getJSONObject("data").toString(4));
                System.out.println(" --------------------- \n");

            } else {
                System.out.println("Error");
                System.out.println("Server responded with: " + con.getResponseCode());
            }


        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    public static void PostExample(){
        try {
            // Construct JSON object
            JSONObject person = new JSONObject();
            person.put("first_name", "Nicholas");
            person.put("last_name", "Lennox");
            person.put("position","Lecturer");

            // Setup connection
            URL url = new URL("https://reqres.in/api/users");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            // Properties needed to perform POST action
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.setRequestProperty("Accept", "application/json");

            // Write data to connection (Output Stream)
            OutputStream os = con.getOutputStream();
            os.write(person.toString().getBytes("UTF-8"));

            // Close Output Stream
            os.close();

            // Check for correct response
            if(con.getResponseCode() == HttpURLConnection.HTTP_CREATED ){

                // Print Response
                InputStreamReader isr = new InputStreamReader(con.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String inputLine;
                StringBuffer content = new StringBuffer();

                while ((inputLine = br.readLine()) != null) {
                    content.append(inputLine);
                }
                br.close();

                // print all content as plain text
                System.out.println(" --- String format --- ");
                System.out.println(content);
                System.out.println(" --------------------- \n");

                // print content formated as JSON
                System.out.println(" ---  JSON format --- ");
                JSONObject json = new JSONObject(content.toString());
                System.out.println(json.toString(4));
                System.out.println(" --------------------- \n");

            } else {
                System.out.println("Error");
                System.out.println("Server responded with: " + con.getResponseCode());
            }


        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
}
